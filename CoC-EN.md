
*This CoC has been collected over time and based on the experience of people in the community.*

Our online events are dedicated to providing discrimination and harassment-free event experiences
for everyone, regardless of gender, age, sexual orientation, disability, race, religion or technology choices.
We do not tolerate racism and harassment of events participants in any form.
Sexual language and imagery is not appropriate for any of our events.
event participants violating these rules may be sanctioned or expelled from the event.

Harassment includes offensive verbal comments related to gender, age, disability, race, religion, technology choices, sexual images and stalking.
Warn: If you are being harassed, notice that someone else is being harassed, or have any other concerns, please contact a member of event organiser immediately.
You will be able to contact us over email via:
**Subject : CoC**
**[info@karajlug.org](mailto:info@karajlug.org)**
